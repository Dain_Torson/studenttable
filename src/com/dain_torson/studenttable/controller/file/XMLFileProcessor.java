package com.dain_torson.studenttable.controller.file;

import com.dain_torson.studenttable.data.Student;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ales on 10.05.2015.
 */
public class XMLFileProcessor {

    private File file;
    private List<Student> students;

    public XMLFileProcessor(File file) {
        this.file = file;
    }

    private Element createStudentNode(Student source, Document document) {

        Element student = document.createElement("student");

        Element firstName = document.createElement("firstName");
        firstName.setTextContent(source.getFirstName());

        Element secondName = document.createElement("secondName");
        secondName.setTextContent(source.getSecondName());

        Element lastName = document.createElement("lastName");
        lastName.setTextContent(source.getLastName());

        Element group = document.createElement("group");
        group.setTextContent(source.getGroup());

        Element socWorks = document.createElement("socWorks");

        for(String work : source.getSocWorks()) {
            Element socWork = document.createElement("work");
            socWork.setTextContent(work);
            socWorks.appendChild(socWork);
        }

        student.appendChild(firstName);
        student.appendChild(secondName);
        student.appendChild(lastName);
        student.appendChild(group);
        student.appendChild(socWorks);

        return student;
    }

    private Student getStudentFromNode(Node source) {

        Element element = (Element) source;
        Student student = new Student();
        student.setFirstName(element.getElementsByTagName("firstName").item(0).getTextContent());
        student.setSecondName(element.getElementsByTagName("secondName").item(0).getTextContent());
        student.setLastName(element.getElementsByTagName("lastName").item(0).getTextContent());
        student.setGroup(element.getElementsByTagName("group").item(0).getTextContent());

        String works [] = new String[Student.numOfSems];
        NodeList workList = element.getElementsByTagName("socWorks").item(0).getChildNodes();

        for(int workIdx = 0; workIdx < works.length; ++workIdx) {
            works[workIdx] = workList.item(workIdx).getTextContent();
        }

        student.setSocWorks(works);

        return student;
    }

    public void save(List<Student> students) {

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document document = docBuilder.newDocument();
            Element root = document.createElement("students");
            document.appendChild(root);

            for(Student student : students) {
                root.appendChild(createStudentNode(student, document));
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(file);

            transformer.transform(source, result);
        }
        catch (ParserConfigurationException exception) {
            exception.printStackTrace();
        }
        catch (TransformerConfigurationException exception) {
            exception.printStackTrace();
        }
        catch (TransformerException exception) {
            exception.printStackTrace();
        }
    }

    public List<Student> open() {

        List<Student> studentList = new ArrayList<Student>();
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document document = docBuilder.parse(file);

            NodeList nodeList = document.getElementsByTagName("student");
            for(int nodeIdx= 0; nodeIdx < nodeList.getLength(); ++nodeIdx) {
                studentList.add(getStudentFromNode(nodeList.item(nodeIdx)));
            }

            return studentList;
        }
        catch (ParserConfigurationException exception) {
            exception.printStackTrace();
        }
        catch (SAXException exception) {
            exception.printStackTrace();
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }

        return null;
    }
}
