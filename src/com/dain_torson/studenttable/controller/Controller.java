package com.dain_torson.studenttable.controller;


import com.dain_torson.studenttable.controller.buttons.FirstPageButtonHandler;
import com.dain_torson.studenttable.controller.buttons.LastPageButtonHandler;
import com.dain_torson.studenttable.controller.buttons.NextButtonEventHandler;
import com.dain_torson.studenttable.controller.buttons.PrevButtonEventHandler;
import com.dain_torson.studenttable.controller.file.XMLFileProcessor;
import com.dain_torson.studenttable.controller.msgbox.AboutMsgBoxController;
import com.dain_torson.studenttable.controller.msgbox.AddRecordMsgBoxController;
import com.dain_torson.studenttable.controller.msgbox.FindRecordMsgBoxController;
import com.dain_torson.studenttable.controller.textfields.RecPerPageFieldListener;
import com.dain_torson.studenttable.data.Student;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class Controller{

    private ObservableList<Student> students = FXCollections.observableArrayList();
    private ObservableList<Student> currentlyVisualised = FXCollections.observableArrayList();
    private static Scene scene;

    private DisplayController displayController;
    private ControlElementsController controlElementsController;

    private RecPerPageFieldListener recPerPageFieldListener;

    @FXML
    private TableView<Student> studentsTable;
    @FXML
    private TableColumn<Student, String> nameColumn;
    @FXML
    private TableColumn<Student, String> groupColumn;
    @FXML
    private List<TableColumn<Student, String>> semColumnList;
    @FXML
    private MenuItem newFileMenuItem;
    @FXML
    private MenuItem openFileMenuItem;
    @FXML
    private MenuItem saveFileMenuItem;
    @FXML
    private MenuItem exitMenuItem;
    @FXML
    private MenuItem addMenuItem;
    @FXML
    private MenuItem searchMenuItem;
    @FXML
    private MenuItem aboutMenuItem;
    @FXML
    private Button prevPageButton;
    @FXML
    private Button nextPageButton;
    @FXML
    private Button firstPageButton;
    @FXML
    private Button lastPageButton;
    @FXML
    private TextField recPerPageField;
    @FXML
    private Button newFileButton;
    @FXML
    private Button openFileButton;
    @FXML
    private Button saveFileButton;
    @FXML
    private Button addRecordButton;
    @FXML
    private Button searchButton;
    @FXML
    private Label pageLabel;

    public static Scene getScene() {
        return scene;
    }

    @FXML
    private void initialize() {

        nameColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("fmlName"));
        groupColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("group"));

        for (int columnIdx = 0; columnIdx < semColumnList.size(); columnIdx++) {
            semColumnList.get(columnIdx).setCellValueFactory(new ArrayPropertyValueFactory(columnIdx));
        }
        displayController = new DisplayController(students, currentlyVisualised, studentsTable);
        controlElementsController = new ControlElementsController(prevPageButton,
                nextPageButton, firstPageButton, lastPageButton, recPerPageField, pageLabel);
        displayController.updateData();
        newFileMenuItem.setOnAction(new NewFileEventHandler());
        openFileMenuItem.setOnAction(new OpenFileEventHandler());
        saveFileMenuItem.setOnAction(new SaveFileEventHandler());
        exitMenuItem.setOnAction(new ExitEventHandler());
        addMenuItem.setOnAction(new AddNewRecordEventHandler());
        searchMenuItem.setOnAction(new SearchForRecordEventHandler());
        aboutMenuItem.setOnAction(new AboutMenuItemHandler());

        newFileButton.setOnAction(new NewFileEventHandler());
        openFileButton.setOnAction(new OpenFileEventHandler());
        saveFileButton.setOnAction(new SaveFileEventHandler());
        addRecordButton.setOnAction(new AddNewRecordEventHandler());
        searchButton.setOnAction(new SearchForRecordEventHandler());

        prevPageButton.setOnAction(new PrevButtonEventHandler(displayController, controlElementsController,
                students));
        nextPageButton.setOnAction(new NextButtonEventHandler(displayController, controlElementsController,
                students));
        firstPageButton.setOnAction(new FirstPageButtonHandler(displayController, controlElementsController,
                students));

        lastPageButton.setOnAction(new LastPageButtonHandler(displayController, controlElementsController,
                students));

        recPerPageFieldListener = new RecPerPageFieldListener(displayController, controlElementsController,
                students);
        recPerPageField.setText(String.valueOf(displayController.getRecordsPerPage()));
        recPerPageField.focusedProperty().addListener(recPerPageFieldListener);
        controlElementsController.updateControls(students.size(), displayController.getPageNumber(),
                displayController.getRecordsPerPage());
    }

    private void initScene() {
        scene = studentsTable.getScene();
    }

    private void resetHandlers() {

        prevPageButton.setOnAction(new PrevButtonEventHandler(displayController, controlElementsController,
                students));
        nextPageButton.setOnAction(new NextButtonEventHandler(displayController, controlElementsController,
                students));
        firstPageButton.setOnAction(new FirstPageButtonHandler(displayController, controlElementsController,
                students));

        lastPageButton.setOnAction(new LastPageButtonHandler(displayController, controlElementsController,
                students));

        recPerPageField.focusedProperty().removeListener(recPerPageFieldListener);
        recPerPageFieldListener = new RecPerPageFieldListener(displayController, controlElementsController,
                students);
        recPerPageField.setText(String.valueOf(displayController.getRecordsPerPage()));
        recPerPageField.focusedProperty().addListener(recPerPageFieldListener);
        controlElementsController.updateControls(students.size(), displayController.getPageNumber(),
                displayController.getRecordsPerPage());

    }


    private class NewFileEventHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            students.clear();
            studentsTable.setItems(students);
            resetHandlers();
        }
    }

    private class SaveFileEventHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save as");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML", "*.xml"));
            fileChooser.setInitialFileName("NewTable.xml");
            File file = fileChooser.showSaveDialog(studentsTable.getScene().getWindow());

            if(file != null) {
                XMLFileProcessor processor = new XMLFileProcessor(file);
                processor.save(students);
            }
        }
    }

    private class OpenFileEventHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open file");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML", "*.xml"));
            File file = fileChooser.showOpenDialog(studentsTable.getScene().getWindow());

            if(file != null) {
                XMLFileProcessor processor = new XMLFileProcessor(file);
                students = FXCollections.observableArrayList(processor.open());
                displayController.setStudentList(students);
                resetHandlers();
                displayController.updateData();
                controlElementsController.updateControls(students.size(), displayController.getPageNumber(),
                        displayController.getRecordsPerPage());
            }

        }
    }

    private class ExitEventHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            Platform.exit();
        }
    }

    private class AddNewRecordEventHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            initScene();
            try {
                AddRecordMsgBoxController.show();
            }
            catch (IOException exception) {
                System.out.println(exception.getMessage());
            }

            Student temp = AddRecordMsgBoxController.getData();
            if(temp != null) {
                students.add(temp);
                displayController.updateData();
                controlElementsController.updateControls(students.size(), displayController.getPageNumber(),
                        displayController.getRecordsPerPage());
            }
        }
    }

    private class SearchForRecordEventHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            initScene();
            try {
                FindRecordMsgBoxController.show(students);
            }
            catch (IOException exception) {
                System.out.println(exception.getMessage());
            }
            displayController.updateData();
            controlElementsController.updateControls(students.size(), displayController.getPageNumber(),
                    displayController.getRecordsPerPage());
        }
    }

    private class AboutMenuItemHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            initScene();
            try {
                AboutMsgBoxController.show();
            }
            catch (IOException exception) {
                System.out.println(exception.getMessage());
            }
        }
    }
}
