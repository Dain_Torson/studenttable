package com.dain_torson.studenttable.controller;

import com.dain_torson.studenttable.data.Student;
import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

/**
 * Created by Ales on 08.05.2015.
 */
public class ArrayPropertyValueFactory implements Callback<TableColumn.CellDataFeatures<Student,String>, ObservableValue<String>> {
    private int socWorkInd;

    public ArrayPropertyValueFactory(int socWorkIndex) {

        this.socWorkInd = socWorkIndex;
    }

    @Override
    public ObservableValue<String> call(TableColumn.CellDataFeatures<Student, String> param) {
        final Student student = param.getValue();

        return new ObservableValue<String>() {
            @Override
            public void addListener(ChangeListener<? super String> listener) {

            }

            @Override
            public void removeListener(ChangeListener<? super String> listener) {

            }

            @Override
            public String getValue() {
                return student.getSocWorks()[socWorkInd];
            }

            @Override
            public void addListener(InvalidationListener listener) {

            }

            @Override
            public void removeListener(InvalidationListener listener) {

            }
        };
    }
}
