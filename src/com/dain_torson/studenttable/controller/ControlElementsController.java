package com.dain_torson.studenttable.controller;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class ControlElementsController {

    private Button prevPageButton;
    private Button nextPageButton;
    private Button firstPageButton;
    private Button lastPageButton;
    private TextField recPerPageField;
    private Label pageLabel;

    public ControlElementsController(Button pPage, Button nPage, Button fPage, Button lPage,
                                     TextField recField, Label pageLabel) {

        this.prevPageButton = pPage;
        this.nextPageButton = nPage;
        this.firstPageButton = fPage;
        this.lastPageButton = lPage;
        this.recPerPageField = recField;
        this.pageLabel = pageLabel;
    }

    public void updateControls(int size, int pageNumber, int recordsPerPage) {

        int maxPageNum = (int)Math.ceil((double)size / recordsPerPage) - 1;
        if(maxPageNum < 0) {
            maxPageNum = 0;
        }
        if(pageNumber == maxPageNum){
            nextPageButton.setDisable(true);
            lastPageButton.setDisable(true);
        }
        else {
            nextPageButton.setDisable(false);
            lastPageButton.setDisable(false);
        }
        if(pageNumber == 0){
            prevPageButton.setDisable(true);
            firstPageButton.setDisable(true);
        }
        else {
            prevPageButton.setDisable(false);
            firstPageButton.setDisable(false);
        }

        pageLabel.setText("Cтраница: " + String.valueOf(pageNumber + 1) + " из " + String.valueOf(maxPageNum + 1));
    }

    public TextField getRecPerPageField() {
        return recPerPageField;
    }
}
