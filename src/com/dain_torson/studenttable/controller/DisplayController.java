package com.dain_torson.studenttable.controller;

import com.dain_torson.studenttable.data.Student;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;

public class DisplayController {

    private int pageNumber = 0;
    private int recordsPerPage = 4;
    private ObservableList<Student> students;
    private ObservableList<Student> currentlyVisualised;
    private TableView<Student> studentsTable;


    public DisplayController(ObservableList<Student> students, ObservableList<Student> visualised,
                             TableView<Student> tableView) {
        this.students = students;
        this.currentlyVisualised = visualised;
        this.studentsTable = tableView;
    }

    public void updateData() {
        currentlyVisualised.clear();
        int start = recordsPerPage * pageNumber;
        for(int recordIdx = start; recordIdx < start + recordsPerPage; ++recordIdx) {
            if(recordIdx >= students.size()) {
                break;
            }
            currentlyVisualised.add(students.get(recordIdx));
        }
        studentsTable.setItems(currentlyVisualised);
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getRecordsPerPage() {
        return recordsPerPage;
    }

    public void setRecordsPerPage(int recordsPerPage) {
        this.recordsPerPage = recordsPerPage;
    }

    public void setStudentList(ObservableList<Student> students) {
        this.students = students;
    }

    public void reset() {
        pageNumber = 0;
        recordsPerPage = 4;
    }
}
