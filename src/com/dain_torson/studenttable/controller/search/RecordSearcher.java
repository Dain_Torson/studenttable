package com.dain_torson.studenttable.controller.search;

import com.dain_torson.studenttable.data.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus on 07.05.2015.
 */
public class RecordSearcher {

    private Student pattern;
    private List<Student> source;
    private int minWorks;
    private int maxWorks;

    public RecordSearcher(Student pattern, List<Student> source, int minWorks, int maxWorks) {
        this.pattern = pattern;
        this.source = source;
        this.minWorks = minWorks;
        this.maxWorks = maxWorks;
    }

    public List<Student> search() {
        List<Student> searchResults = new ArrayList<Student>();

        for(Student student : source) {
            if(student.getLastName().contains(pattern.getLastName()) && student.getGroup().contains(pattern.getGroup())) {
                int totalNum = 0;
                boolean targetWorkMatches = false;
                for (String work : student.getSocWorks()) {
                    if (!work.equals("")) {
                        totalNum++;
                    }
                    if(!targetWorkMatches && work.contains(pattern.getWork(0))) {
                        targetWorkMatches = true;
                    }
                }
                if (totalNum >= minWorks && totalNum <= maxWorks && targetWorkMatches) {
                    searchResults.add(student);
                }
            }
        }
        return searchResults;
    }
}
