package com.dain_torson.studenttable.controller.textfields;

import com.dain_torson.studenttable.controller.ControlElementsController;
import com.dain_torson.studenttable.controller.DisplayController;
import com.dain_torson.studenttable.data.Student;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;

/**
 * Created by asus on 20.05.2015.
 */
public class RecPerPageFieldListener implements ChangeListener<Boolean> {

    private ObservableList<Student> students;
    private DisplayController displayController;
    private ControlElementsController controlElementsController;

    public RecPerPageFieldListener(DisplayController displayController, ControlElementsController controlElementsController,
                                   ObservableList<Student> students) {
        this.displayController = displayController;
        this.controlElementsController = controlElementsController;
        this.students = students;
    }

    @Override
    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
        int recordsPerPage = displayController.getRecordsPerPage();
        try {
            recordsPerPage = Integer.valueOf(controlElementsController.getRecPerPageField().getText());
        } catch (NumberFormatException ex) {
            controlElementsController.getRecPerPageField().setText(String.valueOf(displayController.getRecordsPerPage()));
            return;
        }
        displayController.setRecordsPerPage(recordsPerPage);
        int maxPageNum = (int)Math.ceil((double)students.size() / displayController.getRecordsPerPage()) - 1;
        if(displayController.getPageNumber() > maxPageNum ) {
            displayController.setPageNumber(maxPageNum);
        }
        controlElementsController.updateControls(students.size(), displayController.getPageNumber(),
                displayController.getRecordsPerPage());
        displayController.updateData();
    }
}
