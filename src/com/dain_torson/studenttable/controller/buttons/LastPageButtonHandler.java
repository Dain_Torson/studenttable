package com.dain_torson.studenttable.controller.buttons;

import com.dain_torson.studenttable.controller.ControlElementsController;
import com.dain_torson.studenttable.controller.DisplayController;
import com.dain_torson.studenttable.data.Student;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Created by asus on 20.05.2015.
 */
public class LastPageButtonHandler implements EventHandler<ActionEvent> {

    private ObservableList<Student> students;
    private DisplayController displayController;
    private ControlElementsController controlElementsController;

    public LastPageButtonHandler(DisplayController displayController, ControlElementsController controlElementsController,
                                 ObservableList<Student> students) {
        this.displayController = displayController;
        this.controlElementsController = controlElementsController;
        this.students = students;
    }

    @Override
    public void handle(ActionEvent event) {
        int recordsPerPage = displayController.getRecordsPerPage();
        int pageNumber = (int)Math.ceil((double)students.size() / recordsPerPage) - 1;
        displayController.setPageNumber(pageNumber);
        displayController.updateData();
        controlElementsController.updateControls(students.size(), displayController.getPageNumber(),
                displayController.getRecordsPerPage());
    }
}
