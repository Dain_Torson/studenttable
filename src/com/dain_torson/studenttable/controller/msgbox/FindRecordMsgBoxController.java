package com.dain_torson.studenttable.controller.msgbox;

import com.dain_torson.studenttable.controller.ArrayPropertyValueFactory;
import com.dain_torson.studenttable.controller.ControlElementsController;
import com.dain_torson.studenttable.controller.Controller;
import com.dain_torson.studenttable.controller.DisplayController;
import com.dain_torson.studenttable.controller.buttons.FirstPageButtonHandler;
import com.dain_torson.studenttable.controller.buttons.LastPageButtonHandler;
import com.dain_torson.studenttable.controller.buttons.NextButtonEventHandler;
import com.dain_torson.studenttable.controller.buttons.PrevButtonEventHandler;
import com.dain_torson.studenttable.controller.search.RecordSearcher;
import com.dain_torson.studenttable.controller.textfields.RecPerPageFieldListener;
import com.dain_torson.studenttable.data.Student;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;


public class FindRecordMsgBoxController implements Initializable {


    private static Stage stage = null;
    private static ObservableList<Student> students = FXCollections.observableArrayList();
    private static ObservableList<Student> source;
    private ObservableList<Student> currentlyVisualised = FXCollections.observableArrayList();

    private DisplayController displayController;
    private ControlElementsController controlElementsController;

    private RecPerPageFieldListener recPerPageFieldListener;

    @FXML
    private TextField nameField;
    @FXML
    private TextField groupField;
    @FXML
    private TextField workField;
    @FXML
    private TextField minWorksField;
    @FXML
    private TextField maxWorksField;
    @FXML
    private CheckBox deleteCheckBox;
    @FXML
    private Button searchButton;
    @FXML
    private Button doneButton;
    @FXML
    private TableView<Student> studentsTable;
    @FXML
    private TableColumn<Student, String> nameColumn;
    @FXML
    private TableColumn<Student, String> groupColumn;
    @FXML
    private List<TableColumn<Student, String>> semColumnList;
    @FXML
    private Button prevPageButton;
    @FXML
    private Button nextPageButton;
    @FXML
    private Button firstPageButton;
    @FXML
    private Button lastPageButton;
    @FXML
    private TextField recPerPageField;
    @FXML
    private Label pageLabel;

    public static synchronized void show(ObservableList<Student> list) throws IOException {
        if(stage == null) {
            Parent root = FXMLLoader.load(AddRecordMsgBoxController.class.getResource("../../view/msgbox/findRecordMsgBoxView.fxml"));
            Scene scene = new Scene(root);
            stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("Find");
            stage.initModality(Modality.NONE);
            stage.initOwner(Controller.getScene().getWindow());
        }

        source = list;
        stage.showAndWait();
        stage.toFront();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        nameColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("fmlName"));
        groupColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("group"));

        for(int columnIdx = 0; columnIdx < semColumnList.size(); columnIdx++) {
            semColumnList.get(columnIdx).setCellValueFactory(new ArrayPropertyValueFactory(columnIdx));
        }


        displayController = new DisplayController(students, currentlyVisualised, studentsTable);
        controlElementsController = new ControlElementsController(prevPageButton,
                nextPageButton, firstPageButton, lastPageButton, recPerPageField, pageLabel);
        displayController.updateData();
        studentsTable.setItems(students);

        searchButton.setOnAction(new SearchButtonEventHandler());
        doneButton.setOnAction(new DoneButtonEventHandler());

        prevPageButton.setOnAction(new PrevButtonEventHandler(displayController, controlElementsController,
                students));
        nextPageButton.setOnAction(new NextButtonEventHandler(displayController, controlElementsController,
                students));
        firstPageButton.setOnAction(new FirstPageButtonHandler(displayController, controlElementsController,
                students));

        lastPageButton.setOnAction(new LastPageButtonHandler(displayController, controlElementsController,
                students));

        recPerPageFieldListener = new RecPerPageFieldListener(displayController, controlElementsController,
                students);
        recPerPageField.setText(String.valueOf(displayController.getRecordsPerPage()));
        recPerPageField.focusedProperty().addListener(recPerPageFieldListener);

        controlElementsController.updateControls(students.size(), displayController.getPageNumber(),
                displayController.getRecordsPerPage());
    }


    private void resetHandlers() {

        prevPageButton.setOnAction(new PrevButtonEventHandler(displayController, controlElementsController,
                students));
        nextPageButton.setOnAction(new NextButtonEventHandler(displayController, controlElementsController,
                students));
        firstPageButton.setOnAction(new FirstPageButtonHandler(displayController, controlElementsController,
                students));

        lastPageButton.setOnAction(new LastPageButtonHandler(displayController, controlElementsController,
                students));

        recPerPageField.focusedProperty().removeListener(recPerPageFieldListener);
        recPerPageFieldListener = new RecPerPageFieldListener(displayController, controlElementsController,
                students);
        recPerPageField.setText(String.valueOf(displayController.getRecordsPerPage()));
        recPerPageField.focusedProperty().addListener(recPerPageFieldListener);

        controlElementsController.updateControls(students.size(), displayController.getPageNumber(),
                displayController.getRecordsPerPage());

    }


    public void clear() {

        nameField.clear();
        groupField.clear();
        workField.clear();
        minWorksField.clear();
        maxWorksField.clear();
        students.clear();
        deleteCheckBox.setSelected(false);
        displayController.updateData();
        displayController.reset();
    }

    private class SearchButtonEventHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            Student searchPattern = new Student();
            searchPattern.setLastName(nameField.getText());
            searchPattern.setGroup(groupField.getText());
            searchPattern.setWork(workField.getText(), 0);

            int minNum;
            int maxNum;
            try {
                minNum = Integer.valueOf(minWorksField.getText());
            }
            catch (NumberFormatException exception) {
                minNum = 0;
            }
            try {
                maxNum = Integer.valueOf(maxWorksField.getText());
            }
            catch (NumberFormatException exception) {
                maxNum = 10;
            }

            RecordSearcher searcher = new RecordSearcher(searchPattern, source, minNum, maxNum);
            List<Student> results = searcher.search();
            students = FXCollections.observableArrayList(results);
            resetHandlers();
            displayController.setStudentList(students);

            if(deleteCheckBox.isSelected()) {
                for(Student student : results) {
                    source.remove(student);
                }
            }

            studentsTable.setItems(students);
            displayController.reset();
            displayController.updateData();
            controlElementsController.updateControls(students.size(), displayController.getPageNumber(),
                    displayController.getRecordsPerPage());
        }
    }

    private class DoneButtonEventHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            clear();
            Stage stage = (Stage) doneButton.getScene().getWindow();
            stage.close();
        }
    }
}
