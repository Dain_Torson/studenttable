package com.dain_torson.studenttable.controller.msgbox;

import com.dain_torson.studenttable.controller.Controller;
import com.dain_torson.studenttable.data.Student;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class AddRecordMsgBoxController implements Initializable {

    private static Stage stage = null;
    private static AddRecordMsgBoxController instance = null;
    private static Student data;

    @FXML
    private Button cancelButton;
    @FXML
    private Button okButton;
    @FXML
    private TextField nameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField secondNameField;
    @FXML
    private TextField groupField;
    @FXML
    private ArrayList<TextField> semFieldsList;

    public static synchronized void show() throws IOException{
        if(stage == null) {
            Parent root = FXMLLoader.load(AddRecordMsgBoxController.class.getResource("../../view/msgbox/addRecordMsgBoxView.fxml"));
            Scene scene = new Scene(root);
            stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("Add new record");
            stage.initModality(Modality.NONE);
            stage.initOwner(Controller.getScene().getWindow());
        }

        data = null;
        stage.showAndWait();
        stage.toFront();
    }

    public static AddRecordMsgBoxController getInstance() {
        return instance;
    }

    public static Student getData() {
        return data;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        instance = this;

        okButton.setOnAction(new okButtonEventHandler());
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Stage stage = (Stage) cancelButton.getScene().getWindow();
                stage.close();
            }
        });
    }

    private void clear() {

        nameField.clear();
        lastNameField.clear();
        secondNameField.clear();
        groupField.clear();

        for(TextField textField : semFieldsList) {
            textField.clear();
        }
    }

    private class okButtonEventHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            String name = nameField.getText();
            String lastName = lastNameField.getText();
            String secondName = secondNameField.getText();
            String group = groupField.getText();
            String works [] = new String[Student.numOfSems];

            int index = 0;
            for(TextField field : semFieldsList) {
                works[index] = field.getText();
                index++;
            }

            data = new Student(name, secondName, lastName, group, works);

            clear();
            Stage stage = (Stage) okButton.getScene().getWindow();
            stage.close();
        }
    }
}
